# mosaik SimConfig

The missing implementation of mosaik's SimConfig dictionary.

## Status

[![pipeline status](https://gitlab.com/offis.energy/mosaik/mosaik.simconfig/badges/master/pipeline.svg)](https://gitlab.com/offis.energy/mosaik/mosaik.simconfig/pipelines/)
[![coverage report](https://gitlab.com/offis.energy/mosaik/mosaik.simconfig/badges/master/coverage.svg)](https://gitlab.com/offis.energy/mosaik/mosaik.simconfig/-/jobs)
[![libraries status](https://img.shields.io/librariesio/release/pypi/mosaik.SimConfig)](https://libraries.io/pypi/mosaik.SimConfig)
[![license badge](https://img.shields.io/pypi/l/mosaik.SimConfig)](#)
[![PyPI version](https://img.shields.io/pypi/v/mosaik.SimConfig)](https://pypi.org/project/mosaik.SimConfig/#history)
[![Python Versions](https://img.shields.io/pypi/pyversions/mosaik.SimConfig)](https://pypi.org/project/mosaik.SimConfig)
[![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/2978/badge)](https://bestpractices.coreinfrastructure.org/projects/2978)

## Ensuring PIP

    python -m ensurepip

## Setting up the Virtual Environment

    python -m venv venv

## Installing Requirements

     python -m pip install --upgrade -r requirements.d/venv.txt

## Running the Tests

Under Linux:

    venv/bin/python -m tox

Under Windows:

    venv\Scripts\python.exe -m tox

## Updating the Virtual Environment Requirements

Under Linux:

    venv/bin/python -m pip_review --auto

Under Windows:

    venv\Scripts\python.exe -m pip_review --auto

## Freezing the Virtual Environment Requirements

Under Linux

     venv/bin/python -m pip freeze --all --exclude-editable > requirements.d/venv.txt
     # And remove line beginning with package name

Under Windows

     venv\Scripts\python.exe -m pip freeze --all --exclude-editable > requirements.d/venv.txt
    # And remove line beginning with package name

## Updating the Test Environment Requirements

Under Linux:

    .tox/py36/bin/python -m pip_review --auto

Under Windows:

    venv\Scripts\python.exe -m pip_review --auto

## Freezing the Test Environment Requirements

Under Linux

    .tox/py36/bin/python -m pip freeze --all --exclude-editable > requirements.d/base.txt
    # And remove line beginning with package name

Under Windows

    .tox\py36\Scripts\python -m pip freeze --all --exclude-editable > requirements.d/base.txt
    # And remove line beginning with package name

## Using the Package

    from mosaik_simconfig.simconfig.sim_config import SimConfig
    from tests.mock.simulator import MockSimulator
    
    sim_config = SimConfig()
    sim_config.add_in_process(simulator=MockSimulator)
    
    mosaik.scenario.World(sim_config=sim_config).run(until=1)

For more usage examples, see the tests.

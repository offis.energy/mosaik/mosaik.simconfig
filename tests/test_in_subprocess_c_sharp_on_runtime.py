import os

import mosaik


def test_add_in_subprocess_c_sharp_on_runtime(sim_config):
    sim_config.add_in_subprocess_c_sharp_on_runtime(
        cwd='.',
        env=None,
        runtime='mono',
        simulator_exe='MockSimulator.exe',
    )

    expected = {
        'MockSimulator.exe': {
            'cmd': 'mono MockSimulator.exe %(addr)s',
            'cwd': '.',
            'env': {},
        }
    }

    print(list(sim_config.keys()))

    # Assert current working directory
    assert sim_config['MockSimulator.exe']['cwd'] == \
           expected['MockSimulator.exe']['cwd']

    # Assert command executable
    executable_actual = os.path.splitext(
        os.path.basename(
            sim_config['MockSimulator.exe']['cmd'].split()[0]))[0]
    executable_expected = \
        os.path.splitext(os.path.basename(
            expected['MockSimulator.exe']['cmd'].split()[0]))[0]
    assert executable_actual == executable_expected

    # Assert command address and port
    address_and_port_actual = sim_config['MockSimulator.exe']['cmd'].split()[1]
    address_and_port_expected = expected['MockSimulator.exe']['cmd'].split()[1]
    assert address_and_port_actual == address_and_port_expected

    # Assert environment variables
    assert sim_config['MockSimulator.exe']['env'] == \
           expected['MockSimulator.exe']['env']

    # Use
    world = mosaik.scenario.World(sim_config=sim_config)

    end_in_seconds = 1
    world.run(until=end_in_seconds)

import pytest

from mosaik_simconfig.simconfig.sim_config import SimConfig


@pytest.fixture
def sim_config() -> SimConfig:
    return SimConfig()

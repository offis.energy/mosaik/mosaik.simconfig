from mosaik_simconfig.simconfig.sim_config import SimConfig


def test_instantiation(sim_config):
    assert sim_config is not None
    assert type(sim_config) is SimConfig


def test_length(sim_config):
    assert len(sim_config) == 0

import pytest

from mosaik_simconfig.simconfig.exception import NoAddrTemplateException
from tests.mock import simulator
from tests.mock.simulator import MockSimulator


def test_add_in_subprocess_without_addrs(sim_config):
    with pytest.raises(NoAddrTemplateException):
        sim_config.add_in_subprocess(
            command='python ' + simulator.__file__,
            cwd='.',
            env=None,
            simulator_name=MockSimulator.__name__,
        )

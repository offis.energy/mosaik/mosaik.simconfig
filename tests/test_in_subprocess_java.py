import os

import mosaik


def test_add_in_subprocess_java(sim_config):
    sim_config.add_in_subprocess_java(
        java='java.exe',
        simulator_jar='MockSimulator.jar',
        simulator_class='com.example.java.simulator.MockSimulator'
    )

    expected = {
        'MockSimulator': {
            'cmd': 'java.exe -cp MockSimulator.jar com.example.java.simulator.'
                   'MockSimulator %(addr)s',
            'cwd': '.',
            'env': {},
        }
    }

    # Assert current working directory
    assert sim_config['MockSimulator']['cwd'] == \
           expected['MockSimulator']['cwd']

    # Assert command java executable
    java_actual = os.path.splitext(os.path.basename(
        sim_config['MockSimulator']['cmd'].split()[0]))[0]
    java_expected = os.path.splitext(os.path.basename(
        expected['MockSimulator']['cmd'].split()[0]))[0]
    assert java_actual == java_expected

    # Assert command simulator path
    simulator_path_actual = \
        os.path.basename(sim_config['MockSimulator']['cmd'].split()[1])
    simulator_path_expected = \
        os.path.basename(expected['MockSimulator']['cmd'].split()[1])
    assert simulator_path_actual == simulator_path_expected

    # Assert command address and port
    address_and_port_actual = sim_config['MockSimulator']['cmd'].split()[2]
    address_and_port_expected = expected['MockSimulator']['cmd'].split()[2]
    assert address_and_port_actual == address_and_port_expected

    # Assert environment variables
    assert sim_config['MockSimulator']['env'] == \
           expected['MockSimulator']['env']

    # Use
    world = mosaik.scenario.World(sim_config=sim_config)

    end_in_seconds = 1
    world.run(until=end_in_seconds)

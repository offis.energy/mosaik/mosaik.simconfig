import os

import mosaik

from tests.mock.simulator import MockSimulator


def test_add_in_subprocess_python(sim_config):
    sim_config.add_in_subprocess_python(simulator=MockSimulator)

    import platform

    interpreter = None
    if platform.python_implementation() == 'PyPy':
        interpreter = 'pypy'
    elif platform.python_implementation() == 'CPython':
        interpreter = 'python3'
    if interpreter is None:
        raise NotImplementedError()

    expected = \
        {
            'MockSimulator':
            {
                'cwd': '.',
                'cmd': interpreter + ' '
                       'tests/mock/simulator.py '
                       '%(addr)s',
                'env': {}
            }
        }

    # Assert current working directory
    assert sim_config['MockSimulator']['cwd'] == \
           expected['MockSimulator']['cwd']

    # Assert command python executable
    python_actual = os.path.splitext(os.path.basename(
        sim_config['MockSimulator']['cmd'].split()[0]))[0].rstrip('3')
    python_expected = os.path.splitext(os.path.basename(
        expected['MockSimulator']['cmd'].split()[0]))[0].rstrip('3')
    assert python_actual == python_expected

    # Assert command simulator path
    simulator_path_actual = os.path.basename(
        sim_config['MockSimulator']['cmd'].split()[1])
    simulator_path_expected = os.path.basename(
        expected['MockSimulator']['cmd'].split()[1])
    assert simulator_path_actual == simulator_path_expected

    # Assert command address and port
    address_and_port_actual = sim_config['MockSimulator']['cmd'].split()[2]
    address_and_port_expected = expected['MockSimulator']['cmd'].split()[2]
    assert address_and_port_actual == address_and_port_expected

    # Assert environment variables
    assert sim_config['MockSimulator']['env'] == \
           expected['MockSimulator']['env']

    # Use
    world = mosaik.scenario.World(sim_config=sim_config)

    end_in_seconds = 1
    world.run(until=end_in_seconds)

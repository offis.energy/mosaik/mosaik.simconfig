import mosaik_api

from tests.mock.simulator import MockSimulator


def main():
    return mosaik_api.start_simulation(MockSimulator(), 'Mock Simulator')


if __name__ == '__main__':
    main()

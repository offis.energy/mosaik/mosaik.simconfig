import mosaik

from tests.mock.simulator import MockSimulator


def test_add_in_process(sim_config):
    sim_config.add_in_process(simulator=MockSimulator)

    expected = {
        'MockSimulator': {
            'python': 'tests.mock.simulator:MockSimulator'
        }
    }

    assert sim_config == expected

    world = mosaik.scenario.World(sim_config=sim_config)

    end_in_seconds = 1
    world.run(until=end_in_seconds)
